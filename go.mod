module modernc.org/sqlite

go 1.15

require (
	github.com/mattn/go-sqlite3 v1.14.6
	modernc.org/libc v0.0.0-20210126194511-2b2d365b45c2
	modernc.org/mathutil v1.2.2
	modernc.org/tcl v0.0.0-20210126195340-ef4fe8b071b1
)
